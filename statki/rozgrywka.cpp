#include "stdafx.h"
#include "rozgrywka.h"

bool czyObok(int dl, int x, int y, char **tab, char zn)
{
	bool temp;

	if (zn == 'h')
	{
		for (int i = 0; i < dl; i++)
		{
			if (tab[y][x + i] == '_' && tab[y + 1][x + i] != 'X' && tab[y - 1][x + i] != 'X' && tab[y][x - 1 + i] != 'X' && tab[y][x + i + 1] != 'X')
				temp = false;
			else
				return true;
		}
	}
	else if (zn == 'v')
		for (int i = 0; i < dl; i++)
		{
			if (tab[y + i][x] == '_' && tab[y + i][x + 1] != 'X' && tab[y + i][x - 1] != 'X' && tab[y - 1 + i][x] != 'X' && tab[y + i + 1][x] != 'X')
				temp = false;
			else
				return true;
		}

	return temp;
}

bool czyObok2(int dl, int x, int y, char **tab, char zn, int k)
{
	bool temp;

	if (zn == 'h')
	{
		for (int i = 1; i < dl; i++)
		{
			if ((tab[y][x + i*k] == '_' || tab[y][x + i*k] == 'X') && tab[y + 1][x + i*k] != 'X' && tab[y - 1][x + i*k] != 'X')
				temp = false;
			else
				return true;
		}
	}
	else if (zn == 'v')
		for (int i = 1; i < dl; i++)
		{
			if ((tab[y + i*k ][x] == '_' || tab[y + i*k][x] == 'X') && tab[y + i*k][x + 1] != 'X' && tab[y + i*k][x - 1] != 'X')
				temp = false;
			else
				return true;
		}

	return temp;
}

bool czyBylo(SLista *lista, int x, int y)
{
	SLista *temp = lista;

	while (temp != NULL)
	{
		if (temp->x == x && temp->y == y)
		{
			cout << "Niemozliwe jest wstawienie statku pod podanymi wspolrzednymi!\nPodaj nowe dane." << endl;
			return true;
		}
		temp = temp->next;
	}
	return false;
}

bool poziomo(int dl, int x, int n)
{
	if ((x + dl - 1) <= n)
		return true;
	else
		return false;
}

bool poziomo2(int dl, int x)
{
	if ((x - dl + 1) > 0)
		return true;
	else
		return false;
}

bool pionowo(int dl, int y, int n)
{
	if ((y + dl - 1) <= n)
		return true;
	else
		return false;
}

bool pionowo2(int dl, int y)
{
	if ((y - dl + 1) > 0)
		return true;
	else
		return false;
}

int miejsce(int dl, int x, int y, char **&tab, char zn, int n)
{
	int temp;

	if (zn == 'r')
	{
		temp = rand() % 2;

		if (temp == 1)
		{
			if (poziomo(dl, x, n) == true && czyObok(dl, x, y, tab, 'h') == false)
				return 1;
			else if (pionowo(dl, y, n) == true && czyObok(dl, x, y, tab, 'v') == false)
				return 2;
			else
				return 0;
		}
		else
		{
			if (pionowo(dl, y, n) == true && czyObok(dl, x, y, tab, 'v') == false)
				return 2;
			else if (poziomo(dl, x, n) == true && czyObok(dl, x, y, tab, 'h') == false)
				return 1;
			else
				return 0;
		}
	}
	else if (zn == 'p')
	{
		if (poziomo(dl, x, n) == true && czyObok2(dl, x, y, tab, 'h', 1) == false)
			return 1;
		else
			return 0;
	}
	else if (zn == 'l')
	{
		if (poziomo2(dl, x) == true && czyObok2(dl, x, y, tab, 'h', -1) == false)
			return 1;
		else
			return 0;
	}
	else if (zn == 'g')
	{
		if (pionowo2(dl, y) == true && czyObok2(dl, x, y, tab, 'v', -1) == false)
			return 1;
		else
			return 0;
	}
	else if (zn = 'd')
	{
		if (pionowo(dl, y, n) == true && czyObok2(dl, x, y, tab, 'v', 1) == false)
			return 1;
		else
			return 0;
	}
}

string zapiszWynik(int x, double n)
{
	ofstream wynik;
	string imie;

	wynik.open("rekordy.txt", ios::app);

	cout << "Podaj swoje imie: ";
	cin >> imie;
	wynik << imie << "\t" << x << "\t" << setprecision(2) << n << "%" << endl;

	wynik.close();

	return imie;
}

void dodajNaPlansze(SLista *&lista, char zn, char **&tab, int x, int y, int dl, char kto)
{
	SLista *pop = NULL, *nast = NULL;

	if (zn == 'h')
		for (int i = 0; i < dl; i++)
		{
			tab[y][x + i] = 'X';
			if (kto == 'u')
				dodajE(lista, pop, nast, x + i, y);
		}
	if (zn == 'v')
		for (int i = 0; i < dl; i++)
		{
			tab[y + i][x] = 'X';
			if (kto == 'u')
				dodajE(lista, pop, nast, x, y + i);
		}
}

void dodajStrzal(char **&tab, int x, int y, char zn)
{
	tab[y][x] = zn;
}

void wstawStatekKomp(SStatek **&flota, char **&tab, int n, SInit dane)
{
	int tempX, tempY, kierunek;
	SLista *lista = NULL;

	tempX = rand() % dane.n + 1;
	tempY = rand() % dane.n + 1;

	kierunek = miejsce(dane.s[n], tempX, tempY, tab, 'r', dane.n);

	if (kierunek == 1)
	{
		flota[dane.ile - n - 1] = utworzStatek(dane.s[n], tempX, tempY, 'h');
		dodajNaPlansze(lista, 'h', tab, tempX, tempY, dane.s[n], 'k');
	}
	else if (kierunek == 2)
	{
		flota[dane.ile - n - 1] = utworzStatek(dane.s[n], tempX, tempY, 'v');
		dodajNaPlansze(lista, 'v', tab, tempX, tempY, dane.s[n], 'k');
	}
	else if (kierunek == 0)
		wstawStatekKomp(flota, tab, n, dane);

	usunListe(lista);
}

void wstawStatekUser(SLista *&lista, char **&tab, int n, SInit dane)
{
	int tempX, tempY, kierunek;
	char tempKierunek;
	SLista *pop = NULL, *nast = NULL;

	do
	{
		cout << "Podaj kierunek wstawiania statku (h - poziomo, v - pionowo): ";
		cin >> tempKierunek;
		cout << "Podaj wspolrzedna x: ";
		cin >> tempX;
		cout << "Podaj wspolrzedna y: ";
		cin >> tempY;
	} while ((tempX > dane.n || tempY > dane.n) || (tempX <= 0 || tempY <= 0) || (tempKierunek != 'h' && tempKierunek != 'v') || czyBylo(lista, tempX, tempY) == true);

	dodajE(lista, pop, nast, tempX, tempY);

	if (tempKierunek == 'h')
	{
		if (poziomo(dane.s[n], tempX, dane.n) == true && czyObok(dane.s[n], tempX, tempY, tab, 'h') == false)
			kierunek = 1;
		else if (pionowo(dane.s[n], tempY, dane.n) == true && czyObok(dane.s[n], tempX, tempY, tab, 'v') == false)
			kierunek = 2;
		else
			kierunek = 0;
	}
	else if (tempKierunek == 'v')
	{
		if (pionowo(dane.s[n], tempY, dane.n) == true && czyObok(dane.s[n], tempX, tempY, tab, 'v') == false)
			kierunek = 2;
		else if (poziomo(dane.s[n], tempX, dane.n) == true && czyObok(dane.s[n], tempX, tempY, tab, 'h') == false)
			kierunek = 1;
		else
			kierunek = 0;
	}

	if (kierunek == 1)
		dodajNaPlansze(lista, 'h', tab, tempX, tempY, dane.s[n], 'u');
	else if (kierunek == 2)
		dodajNaPlansze(lista, 'v', tab, tempX, tempY, dane.s[n], 'u');
	else if (kierunek == 0)
	{
		cout << "Niemozliwe jest wstawienie statku pod podanymi wspolrzednymi!\nPodaj nowe dane." << endl;
		wstawStatekUser(lista, tab, n, dane);
	}
}

void wstawFloteKomp(SStatek **&flota, char **&tab, SInit dane)
{
	for (int i = 0; i < dane.ile; i++)
		wstawStatekKomp(flota, tab, i, dane);
}

void wstawFloteUser(char**&tab, SInit dane)
{
	SLista *lista = NULL;

	for (int i = 0; i < dane.ile; i++)
	{
		system("cls");
		rysujPlansze(tab, dane.n + 2);
		cout << "Wstaw " << dane.s[i] << "-masztowiec" << endl;
		wstawStatekUser(lista, tab, i, dane);
	}

	usunListe(lista);
}

void zapiszPlansze(int n)
{
	ofstream plansza;

	plansza.open("plansza.txt");
	
	switch (n)
	{
		case 1: plansza << "6" << endl;//wielko�� planszy n x n
				plansza << "4" << endl;//ilo�� statk�w
				plansza << "3\t2\t1\t1" << endl;//d�ugo�� poszczeg�lnych statk�w
				break;
		/*case 1: plansza << "4" << endl;//wielko�� planszy n x n
				plansza << "3" << endl;//ilo�� statk�w
				plansza << "2\t1\t1" << endl;//d�ugo�� poszczeg�lnych statk�w
				break;*/
		case 2: plansza << "8" << endl;//wielko�� planszy n x n
				plansza << "5" << endl;//ilo�� statk�w
				plansza << "4\t3\t2\t2\t1" << endl;//d�ugo�� poszczeg�lnych statk�w
				break;
		case 3: plansza << "10" << endl;//wielko�� planszy n x n
				plansza << "6" << endl;//ilo�� statk�w
				plansza << "5\t4\t3\t3\t2\t1" << endl;//d�ugo�� poszczeg�lnych statk�w
				break;
	}

	plansza.close();
}

void init(int n, char **&tabPlanszaKomp, char **&tabPlanszaUser, char **&tabStrzalyKomp, char **&tabStrzalyUser, SStatek **&flota, SInit &dane)
{
	ifstream plansza;
	
	plansza.open("plansza.txt");

	plansza >> dane.n;
	plansza >> dane.ile;
	dane.s = new int[dane.ile];
	for (int i = 0; i < dane.ile; i++)
	{
		plansza >> dane.s[i];
		dane.pkt += dane.s[i];
	}

	flota = new SStatek*[dane.ile];
	utworzPlansze(tabPlanszaKomp, dane.n + 2);
	//utworz(tabPlanszaKomp, dane.n);
	wypelnijPlansze(tabPlanszaKomp, dane.n + 2);
	//wypelnij(tabPlanszaKomp, dane.n);
	wstawFloteKomp(flota, tabPlanszaKomp, dane);
	//rysujPlansze(tabPlanszaKomp, dane.n + 2);
	//utworzPlansze(tabPlanszaUser, dane.n + 2);
	//wypelnijPlansze(tabPlanszaUser, dane.n + 2);
	utworzPlansze(tabStrzalyUser, dane.n + 2);
	//utworz(tabStrzalyUser, dane.n);
	wypelnijPlansze(tabStrzalyUser, dane.n + 2);
	//wypelnij(tabStrzalyUser, dane.n);
	utworzPlansze(tabStrzalyKomp, dane.n + 2);
	//utworz(tabStrzalyKomp, dane.n);
	wypelnijPlansze(tabStrzalyKomp, dane.n + 2);
	//wypelnij(tabStrzalyKomp, dane.n);
	
	//system("pause");
	
	plansza.close();
}

char rzutMoneta()
{
	int temp;
	char user, kto;

	system("cls");
	baner("STATKI");
	do
	{
		cout << "Rzuc moneta by wybrac zaczynajacego.\nOrzel czy reszka (O/R)? ";
		cin >> user;
	} while (user != 'O' && user != 'o' && user != 'r' && user != 'R');

	temp = rand() % 2;
	if (temp == 1)
	{
		cout << "Wylosowano: ORZEL, ";
		if (user == 'O' || user == 'o')
		{
			cout << "zaczynasz!" << endl;
			kto = 'u';
		}
		else if (user == 'R' || user == 'r')
		{
			cout << "zaczyna komputer!" << endl;
			kto = 'c';
		}
	}
	else if (temp == 0)
	{
		cout << "Wylosowano: RESZKA, ";
		if (user == 'R' || user == 'r')
		{
			cout << "zaczynasz!" << endl;
			kto = 'u';
		}
		else if (user == 'O' || user == 'o')
		{
			cout << "zaczyna komputer!" << endl;
			kto = 'c';
		}
	}
	system("pause");

	return kto;
}

bool czyOstatni(SStatek *statek)
{
	int ileS = 0, ileT = 0;
	SStatek *temp = statek;

	while (temp != NULL)
	{
		if (temp->trafiony == true)
			ileT++;
		ileS++;

		temp = temp->next;
	}

	if (ileS == ileT)
		return true;
	else
		return false;
}

int ktoryStatek(SStatek **&flota, int x, int y, int n)
{
	int ktory;

	for (int i = 0; i < n; i++)
	{
		SStatek *temp = flota[i];
		while (temp != NULL)
		{
			if (temp->x == x && temp->y == y)
			{
				temp->trafiony = true;
				ktory = i;
				break;
			}
			temp = temp->next;
		}
	}

	return ktory;
}

void planDzialania(char **tab, int x, int y, SLista **&plan, SInit dane)
{
	SLista *prawo = NULL, *lewo = NULL, *gora = NULL, *dol = NULL;

	for (int i = 2; i <= dane.ile; i++)
	{
		if (miejsce(i, x, y, tab, 'p', dane.n) == 1)
		{
			usunListe(prawo);
			utworzListe(prawo, i, x, y, 'h', 1);
		}
		if (miejsce(i, x, y, tab, 'l', dane.n) == 1)
		{
			usunListe(lewo);
			utworzListe(lewo, i, x, y, 'h', -1);
		}
		if (miejsce(i, x, y, tab, 'g', dane.n) == 1)
		{
			usunListe(gora);
			utworzListe(gora, i, x, y, 'v', -1);
		}
		if (miejsce(i, x, y, tab, 'd', dane.n) == 1)
		{
			usunListe(dol);
			utworzListe(dol, i, x, y, 'v', 1);
		}
	}

	plan[0] = prawo;
	plan[1] = lewo;
	plan[2] = gora;
	plan[3] = dol;
}

bool oszustwo()
{
	bool temp = false;

	cout << "oszustwo" << endl;

	/*
	I - u�ytkownik podaje trafienia dw�ch r�nych statk�w obok siebie, bez zachowania 1-kom�rkowej przerwy
	II - u�ytkownik ujawnia mniej trafie� ni� powinno by�
	*/
	
	return temp;
}

void computerShot(SLista *&lista, SLista *&pop, SLista *&nast, int &pkt, char **&tabToComp, char **&tabUser, SLista **&plan, char **&tabToUser, SPlanD &planD, SLista *&statek, SLista *&popStatek, SLista *&nastStatek, SInit dane, SWynik &wynik)
{
	int x, y;
	string damage;
	SLista *temp = NULL;
	char kierunek = '-';

	baner("STATKI");
	rysujWszystko(tabToComp, tabUser, dane.n);
	baner("KOMPUTER");
	cout << "GRACZ - aktualny wynik: " << wynik.wynik << " oraz skutecznosc: " << setprecision(2) << wynik.skutecznosc << "%" << endl;

	if (planD.plan == true)
	{
		temp = plan[planD.kierunek];

		do
		{
			if (temp != NULL)
			{
				x = temp->x;
				y = temp->y;

				temp = temp->next;
			}
			else
			{
				do
				{
					planD.kierunek = rand() % 4;
				} while (plan[planD.kierunek] == NULL);

				temp = plan[planD.kierunek];
			}
		} while (czyBylo(lista, x, y) == true);
	}
	else
	{
		do
		{
			x = rand() % dane.n + 1;
			y = rand() % dane.n + 1;
		} while (czyBylo(lista, x, y) == true);
	}

	dodajE(lista, pop, nast, x, y);

	system("cls");
	baner("STATKI");
	rysujWszystko(tabToComp, tabUser, dane.n);
	baner("KOMPUTER");
	cout << "GRACZ - aktualny wynik: " << wynik.wynik << " oraz skutecznosc: " << setprecision(2) << wynik.skutecznosc << "%" << endl;

	cout << "Komputer oddal strzal w podane wspolrzedne: (" << x << "," << y << ")" << endl;
	
	do
	{
		cout << "Okresl stopien zniszczen: ";
		cin >> damage;

		for (int i = 0; i < damage.size(); i++)
			damage[i] = tolower(damage[i]);
	} while (damage != "pudlo" && damage != "trafiony" && damage != "zatopiony");

	if (damage == "pudlo")
	{
		dodajStrzal(tabUser, x, y, 'O');
		dodajStrzal(tabToUser, x, y, 'O');
		
		if (planD.plan == true)
		{
			usunListe(plan[planD.kierunek]);

			do
			{
				planD.kierunek = rand() % 4;
			} while (plan[planD.kierunek] == NULL);
		}

		wynik.wynik += 10;
	}
	else if (damage == "trafiony")
	{
		dodajStrzal(tabUser, x, y, 219);
		dodajStrzal(tabToUser, x, y, 'X');
		pkt++;

		dodajE(statek, popStatek, nastStatek, x, y);
		if (planD.plan == true)
		{
			if (statek->next != NULL)
			{
				if (statek->x == (statek->next)->x)
				{
					kierunek = 'v';
					usunListe(plan[0]);
					usunListe(plan[1]);
					plan[0] = NULL;
					plan[1] = NULL;
				}
				else if (statek->y == (statek->next)->y)
				{
					kierunek = 'h';
					usunListe(plan[2]);
					usunListe(plan[3]);
					plan[2] = NULL;
					plan[3] = NULL;
				}
			}
		}

		if (planD.strzal == false)
		{
			planDzialania(tabToUser, x, y, plan, dane);

			planD.strzal = true;
			planD.plan = true;

			do
			{
				planD.kierunek = rand() % 4;
			} while (plan[planD.kierunek] == NULL);
		}
	}
	else if (damage == "zatopiony")
	{
		dodajStrzal(tabUser, x, y, 219);
		dodajStrzal(tabToUser, x, y, 'X');
		pkt++;

		dodajE(statek, popStatek, nastStatek, x, y);
		dodajOtoczenie(lista, pop, nast, statek, tabToUser);
		usunListe(statek);
		popStatek = NULL;
		nastStatek = NULL;

		planD.plan = false;
		planD.strzal = false;
		
		for (int i = 0; i < 4; i++)
			usunListe(plan[i]);
		plan = new SLista*[4];
		for (int i = 0; i < 4; i++)
			plan[i] = NULL;
	}

	system("cls");
	baner("STATKI");
	rysujWszystko(tabToComp, tabUser, dane.n);
	baner("KOMPUTER");
	cout << "GRACZ - aktualny wynik: " << wynik.wynik << " oraz skutecznosc: " << setprecision(2) << wynik.skutecznosc << "%" << endl;
	cout << "Gramy dalej!" << endl;

	//rysujPlansze(tabToUser, dane.n + 2);

	system("pause");
}

void userShot(SStatek **flota, SLista *&lista, SLista *&pop, SLista *&nast, int &pkt, char **&tabToComp, char **&tabUser, char **tabComp, SInit dane, SWynik &wynik)
{
	int x, y, statek;
	bool zatopiony = false;
	string damage;

	baner("STATKI");
	rysujWszystko(tabToComp, tabUser, dane.n);
	baner("GRACZ");
	cout << "GRACZ - aktualny wynik: " << wynik.wynik << " oraz skutecznosc: " << setprecision(2) << wynik.skutecznosc << "%" << endl;

	do
	{
		cout << "Podaj wspolrzedna x: ";
		cin >> x;
		cout << "Podaj wspolrzedna y: ";
		cin >> y;
	} while ((x > dane.n || y > dane.n) || (x <= 0 || y <= 0) || czyBylo(lista, x, y) == true);

	dodajE(lista, pop, nast, x, y);
	wynik.strzaly++;

	if (tabComp[y][x] == 'X')
	{
		statek = ktoryStatek(flota, x, y, dane.ile);
		zatopiony = czyOstatni(flota[statek]);
	}

	if (tabComp[y][x] == 'X' && zatopiony != true)
	{
		damage = "trafiony!";
		pkt++;
		dodajStrzal(tabToComp, x, y, 219);
		wynik.wynik += 50;
		wynik.trafione++;
	}
	else if (tabComp[y][x] == 'X' && zatopiony == true)
	{
		damage = "zatopiony!";
		pkt++;
		dodajStrzal(tabToComp, x, y, 219);
		wynik.wynik += 100;
		wynik.trafione++;
	}
	else
	{
		damage = "pudlo!";
		dodajStrzal(tabToComp, x, y, 'O');
	}

	wynik.skutecznosc = (wynik.trafione / wynik.strzaly) * 100;

	system("cls");
	baner("STATKI");
	rysujWszystko(tabToComp, tabUser, dane.n);
	baner("GRACZ");
	cout << "GRACZ - aktualny wynik: " << wynik.wynik << " oraz skutecznosc: " << setprecision(2) << wynik.skutecznosc << "%" << endl;

	cout << "Oddales strzal w podane wspolrzedne: (" << x << "," << y << ")" << endl;
	cout << "Stopien zniszczen: " << damage << endl;

	system("pause");
}

void rozgrywka(int n)
{
	int pktU = 0, pktC = 0;
	char **tabPlanszaKomp = NULL, **tabPlanszaUser = NULL, **tabStrzalyUser = NULL, **tabStrzalyKomp = NULL;
	SStatek **flota, **temp;
	SLista *listaComp = NULL, *popComp = NULL, *nastComp = NULL, *listaUser = NULL, *popUser = NULL, *nastUser = NULL, **plan = new SLista*[4], *statek = NULL, *popStatek = NULL, *nastStatek = NULL;
	SPlanD planD;
	SInit dane;
	SWynik wynik;

	for (int i = 0; i < 4; i++)
		plan[i] = NULL;

	zapiszPlansze(n);
	init(n, tabPlanszaKomp, tabPlanszaUser, tabStrzalyKomp, tabStrzalyUser, flota, dane);
	temp = new SStatek*[dane.ile];

	system("cls");
	wstawStatki(tabPlanszaUser);
	//wstawFloteUser(tabPlanszaUser,dane);
	//wstawFloteKomp(temp, tabPlanszaUser, dane);
	system("cls");
	//rysujPlansze(tabPlanszaUser, dane.n + 2);
	//system("pause");

	//rozgrywka w�a�ciwa
	if (rzutMoneta() == 'u')
	{
		while (pktC < dane.pkt && pktU < dane.pkt)
		{
			system("cls");
			userShot(flota, listaUser, popUser, nastUser, pktU, tabStrzalyUser, tabPlanszaUser, tabPlanszaKomp, dane, wynik);
			system("cls");
			if (pktU >= dane.pkt)
				break;
			else
				computerShot(listaComp, popComp, nastComp, pktC, tabStrzalyUser, tabPlanszaUser, plan, tabStrzalyKomp, planD, statek, popStatek, nastStatek, dane, wynik);
		}
	}
	else
	{
		while (pktC < dane.pkt && pktU < dane.pkt)
		{
			system("cls");
			computerShot(listaComp, popComp, nastComp, pktC, tabStrzalyUser, tabPlanszaUser, plan, tabStrzalyKomp, planD, statek, popStatek, nastStatek, dane, wynik);
			system("cls");
			if (pktC >= dane.pkt)
				break;
			else
				userShot(flota, listaUser, popUser, nastUser, pktU, tabStrzalyUser, tabPlanszaUser, tabPlanszaKomp, dane, wynik);
		}
	}
	//------------------------------

	system("cls");
	baner("STATKI");
	cout << "Koniec gry!" << endl;
	if (pktU > pktC)
	{
		cout << "Gratulacje, wygrales!" << endl;
		cout << zapiszWynik(wynik.wynik, wynik.skutecznosc) << ", Twoj wynik to: " << wynik.wynik << " przy " << wynik.skutecznosc << "% skutecznosci!" << endl;
	}
	else
	{
		cout << "Przegrales, sprobuj jeszcze raz." << endl;
		cout << zapiszWynik(wynik.wynik, wynik.skutecznosc) << ", Twoj wynik to: " << wynik.wynik << " przy " << setprecision(2) << wynik.skutecznosc << "% skutecznosci!" << endl;
	}
	
	system("pause");

	usunFlote(flota, dane.ile);
	usunListe(listaComp);
	usunListe(listaUser);
	usunListe(statek);
	for (int i = 0; i < 4; i++)
		usunListe(plan[i]);
	delete plan;
}
