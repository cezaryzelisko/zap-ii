#ifndef LISTA
#define LISTA

#include <iostream>
using namespace std;

struct SLista
{
	int x, y;
	SLista *next;
};

void utworzListe(SLista *&lista, int dl, int x, int y, char zn, int kierunek);
void dodajE(SLista *&lista, SLista *&pop, SLista *&nast, int x, int y);
void dodajOtoczenie(SLista *&lista, SLista *&pop, SLista *&nast, SLista *statek, char **&tab);
void usunListe(SLista *&lista);
void drukujListe(SLista *lista);

#endif // LISTA
