#include "stdafx.h"
#include "lista.h"
//#include "rozgrywka.h"
//#include "plansza.h"

void utworzListe(SLista *&lista, int dl, int x, int y, char zn, int kierunek)
{
	SLista *pop = NULL, *nast = NULL;

	if (kierunek == 1)
	{
		for (int i = 0; i < dl; i++)
		{
			pop = nast;
			nast = new SLista;
			if (zn == 'h')
			{
				nast->x = x + i;
				nast->y = y;
			}
			else if (zn == 'v')
			{
				nast->x = x;
				nast->y = y + i;
			}
			nast->next = NULL;

			if (pop == NULL)
				lista = nast;
			else
				pop->next = nast;
		}
	}
	else if (kierunek == -1)
	{
		for (int i = 0; i < dl; i++)
		{
			pop = nast;
			nast = new SLista;
			if (zn == 'h')
			{
				nast->x = x - i;
				nast->y = y;
			}
			else if (zn == 'v')
			{
				nast->x = x;
				nast->y = y - i;
			}
			nast->next = NULL;

			if (pop == NULL)
				lista = nast;
			else
				pop->next = nast;
		}
	}
}

void dodajE(SLista *&lista, SLista *&pop, SLista *&nast, int x, int y)
{
	pop = nast;
	nast = new SLista;
	nast->x = x;
	nast->y = y;
	nast->next = NULL;

	if (pop == NULL)
		lista = nast;
	else
		pop->next = nast;
}

void dodajOtoczenie(SLista *&lista, SLista *&pop, SLista *&nast, SLista *statek, char **&tab)
{
	int pom = 0, ile = 0;
	char kierunek;
	SLista *temp = statek;

	while (temp != NULL)
	{
		if (tab[temp->y][temp->x - 1] != 'X')//lewo
		{
			dodajE(lista, pop, nast, temp->x - 1, temp->y);
			tab[temp->y][temp->x - 1] = 'O';
		}

		if (tab[temp->y][temp->x + 1] != 'X')//prawo
		{
			dodajE(lista, pop, nast, temp->x + 1, temp->y);
			tab[temp->y][temp->x + 1] = 'O';
		}

		if (tab[temp->y - 1][temp->x] != 'X')//g�ra
		{
			dodajE(lista, pop, nast, temp->x, temp->y - 1);
			tab[temp->y - 1][temp->x] = 'O';
		}

		if (tab[temp->y + 1][temp->x] != 'X')//d�
		{
			dodajE(lista, pop, nast, temp->x, temp->y + 1);
			tab[temp->y + 1][temp->x] = 'O';
		}
		temp = temp->next;
	}
}

void usunListe(SLista *&lista)
{
	SLista *temp;

	while (lista != NULL)
	{
		temp = lista;
		lista = lista->next;
		delete temp;
	}

	lista = NULL;
}

void drukujListe(SLista *lista)
{
	SLista *temp = lista;

	while (temp != NULL)
	{
		cout << "(" << temp->x << "," << temp->y << ")\t";
		temp = temp->next;
	}
	cout << endl;
}
