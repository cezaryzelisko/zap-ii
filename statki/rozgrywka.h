#ifndef ROZGRYWKA
#define ROZGRYWKA

#include <iostream>
#include "lista.h"
#include "plansza.h"
#include "menu.h"
#include "wstawStatek.h"
using namespace std;

struct SPlanD
{
	int kierunek;
	bool plan = false;
	bool strzal = false;
};

struct SInit
{
	int n;
	int ile;
	int *s;
	int pkt=0;
};

struct SWynik
{
	int wynik = 0;
	double strzaly = 0;
	double trafione = 0;
	double skutecznosc = 0;
};

bool czyObok(int dl, int x, int y, char **tab, char zn);
bool czyObok2(int dl, int x, int y, char **tab, char zn, int k);
bool czyBylo(SLista *lista, int x, int y);
bool poziomo(int dl, int x, int n);
bool poziomo2(int dl, int x);
bool pionowo(int dl, int y, int n);
bool pionowo2(int dl, int y);
int miejsce(int dl, int x, int y, char **&tab, char zn, int n);
string zapiszWynik(int x, double n);
void dodajNaPlansze(SLista *&lista, char zn, char **&tab, int x, int y, int dl, char kto);
void dodajStrzal(char **&tab, int x, int y, char zn);
void wstawStatekKomp(SStatek **&flota, char **&tab, int n, SInit dane);
void wstawStatekUser(SLista *&lista, char **&tab, int n, SInit dane);
void wstawFloteKomp(SStatek **&flota, char **&tab, SInit dane);
void wstawFloteUser(char**&tab, SInit dane);
void zapiszPlansze(int n);
void init(int n, char **&tabPlanszaKomp, char **&tabPlanszaUser, char **&tabStrzalyKomp, char **&tabStrzalyUser, SStatek **&flota, SInit &dane);
char rzutMoneta();
bool czyOstatni(SStatek *statek);
int ktoryStatek(SStatek **&flota, int x, int y, int n);
void planDzialania(char **tab, int x, int y, SLista **&plan, SInit dane);
bool oszustwo();
void computerShot(SLista *&lista, SLista *&pop, SLista *&nast, int &pkt, char **&tabComp, char **&tabUser, SLista **&plan, char **&tabToUser, SPlanD &planD, SLista *&statek, SLista *&popStatek, SLista *&nastStatek, SInit dane, SWynik &wynik);
void userShot(SStatek **flota, SLista *&lista, SLista *&pop, SLista *&nast, int &pkt, char **&tabToComp, char **&tabUser, char **tabComp, SInit dane, SWynik &wynik);
void rozgrywka(int n);

#endif // !ROZGRYWKA
