#ifndef WSTAWSTATEK
#define WSTAWSTATEK

#include <iostream>
using namespace std;

struct SKursor
{
	int x = 0;
	int y = 0;
};

struct SDane
{
	int n;
	int ile;
	int *s;
	int pkt = 0;
};

void utworz(char **&tab, int n);
void wypelnij(char **&tab, int n);
void rysuj(char **tab, int n);
void zapiszDane(int n);
void odczytDanych(SDane &dane);
void dodajZnak(char **&tab, int x, int y, char zn);
void przesun(char **&tab, int &x, int &y, SKursor &temp, int &tempX, int &tempY, int n);
bool otoczenie(char **tab, int tempX, int tempY, int x, int y, int n);
void wstawStatki(char **&plansza);

#endif // !WSTAWSTATEK
