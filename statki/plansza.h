#ifndef PLANSZA
#define PLANSZA

#include <iostream>
using namespace std;

struct SStatek
{
	int x, y;
	bool trafiony = false;
	SStatek *pierwszy;
	SStatek *next;
};

void utworzPlansze(char **&tab, int n);
void wypelnijPlansze(char **&tab, int n);
void rysujPlansze(char **tab, int n);
void rysujWszystko(char **tabComp, char **tabUser, int n);
SStatek *utworzStatek(int n, int x, int y, char zn);
void usunStatek(SStatek *&statek);
void drukujStatek(SStatek *statek);
void drukujFlote(SStatek **flota, int n);
void usunFlote(SStatek **&flota, int n);

#endif // !PLANSZA
