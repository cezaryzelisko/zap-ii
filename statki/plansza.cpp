#include "stdafx.h"
#include "plansza.h"
#include "menu.h"

void utworzPlansze(char **&tab, int n)
{
	tab = new char*[n];
	for (int i = 0; i < n; i++)
		tab[i] = new char[n];
}

void wypelnijPlansze(char **&tab, int n)
{
	//obramowanie
	//naro�niki
	tab[0][0] = 201;
	tab[0][n - 1] = 187;
	tab[n - 1][n - 1] = 188;
	tab[n - 1][0] = 200;
	for (int i = 1; i < n - 1; i++)
		tab[0][i] = 205;
	for (int i = 1; i < n - 1; i++)
		tab[i][n - 1] = 186;
	for (int i = 1; i < n - 1; i++)
		tab[n - 1][i] = 205;
	for (int i = 1; i < n - 1; i++)
		tab[i][0] = 186;
	//-------------------------
	//�rodek
	for (int i = 1; i < n - 1; i++)
		for (int j = 1; j < n - 1; j++)
			tab[i][j] = '_';
}

void rysujPlansze(char **tab, int n)
{
	baner("STATKI");
	cout << setw(2) << " ";
	for (int i = 0; i < n - 1; i++)
	{
		if (i != (n - 2))
			cout << setw(1) << i << "|";
		else
			cout << setw(1) << i;
	}
	cout << endl;

	for (int i = 0; i < n; i++)
	{
		if (i != (n - 1))
			cout << setw(2) << i;
		else
			cout << setw(2) << " ";
		for (int j = 0; j < n; j++)
		{
			if (j != (n - 1))
				cout << setw(1) << tab[i][j] << "|";
			else
				cout << setw(1) << tab[i][j];
		}
		cout << endl;
	}
}

void rysujWszystko(char **tabComp, char **tabUser, int n)
{
	cout << "STRZALY NA PLANSZE KOMPUTERA";
	cout << setw(15) << " " << "STRZALY NA TWOJA PLANSZE";
	cout << endl;

	cout << setw(3) << "|";
	for (int i = 0; i < n; i++)
		cout << i << setw(1) << "|";
	cout << setw(23 + (10 - n/2)) << "|";
	for (int i = 0; i < n; i++)
		cout << i << setw(1) << "|";
	cout << endl;

	for (int i = 0; i < n; i++)
	{
		cout << setw(2) << i << setw(1) << "|";
		for (int j = 1; j < n + 1; j++)
			cout << tabComp[i + 1][j] << "|";
		cout << setw(22 + (10 - n/2)) << i << setw(1) << "|";
		for (int j = 0; j < n; j++)
			cout << tabUser[i][j] << "|";
		cout << endl;
	}

	/*
	cout << setw(2) << " ";
	for (int i = 0; i < n - 1; i++)
	{
		if (i != (n - 2))
			cout << setw(1) << i << "|";
		else
			cout << setw(1) << i;
	}
	if (n == 8 || n == 10)
		cout << setw(22) << " ";
	else
		cout << setw(21) << " ";
	for (int i = 0; i < n - 1; i++)
	{
		if (i != (n - 2))
			cout << setw(1) << i << "|";
		else
			cout << setw(1) << i;
	}
	cout << endl;

	for (int i = 0; i < n; i++)
	{
		if (i != (n - 1))
			cout << setw(2) << i;
		else
			cout << setw(2) << " ";
		for (int j = 0; j < n; j++)
		{
			if (j != (n - 1))
				cout << setw(1) << tabComp[i][j] << "|";
			else
				cout << setw(1) << tabComp[i][j];
		}

		if (i != (n - 1))
			cout << setw(20) << i;
		else
			cout << setw(20) << " ";
		for (int j = 0; j < n; j++)
		{
			if (j != (n - 1))
				cout << setw(1) << tabUser[i][j] << "|";
			else
				cout << setw(1) << tabUser[i][j];
		}
		cout << endl;
	}
	*/
}



SStatek *utworzStatek(int n, int x, int y, char zn)
{
	SStatek *glowa = NULL, *akt = NULL, *pop = NULL;

	for (int i = 0; i < n; i++)
	{
		pop = akt;
		akt = new SStatek;
		if (zn == 'h')
		{
			akt->x = x + i;
			akt->y = y;
		}
		else if (zn == 'v')
		{
			akt->x = x;
			akt->y = y + i;
		}

		akt->next = NULL;
		akt->pierwszy = glowa;

		if (pop == NULL)
		{
			glowa = akt;
			glowa->pierwszy = glowa;
		}
		else
			pop->next = akt;
	}
	return glowa;
}

void usunStatek(SStatek *&statek)
{
	SStatek *usun = NULL;
	while (statek != NULL)
	{
		usun = statek;
		statek = statek->next;
		delete usun;
	}
}

void drukujStatek(SStatek *statek)
{
	while (statek != NULL)
	{
		cout << "(" << statek->x << "," << statek->y << ")\t";
		cout << "glowa: (" << statek->pierwszy->x << "," << statek->pierwszy->y << ")\t";
		statek = statek->next;
	}
}

void drukujFlote(SStatek **flota, int n)
{
	for (int i = 0; i < n; i++)
	{
		drukujStatek(flota[i]);
		cout << endl;
	}
}

void usunFlote(SStatek **&flota, int n)
{
	for (int i = 0; i < n; i++)
		usunStatek(flota[i]);
}
