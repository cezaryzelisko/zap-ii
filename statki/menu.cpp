#include "stdafx.h"
#include "menu.h"
#include "rozgrywka.h"

void baner(string kto)
{
	for (int i = 0; i < 80; i++)
		cout << "-";
	for (int i = 0; i < 35; i++)
		cout << "-";
	cout << " " << setw(8) << kto << setw(1) << " ";
	for (int i = 0; i < 35; i++)
		cout << "-";
	for (int i = 0; i < 80; i++)
		cout << "-";
}

void showMenu()
{
	baner("STATKI");
	cout << endl << endl << "\t\t";
	cout << "1. Nowa gra" << endl << "\t\t";
	cout << "2. Tablica rekordow" << endl << "\t\t";
	cout << "3. Instrukcja" << endl << "\t\t";
	cout << "4. Koniec" << endl;
}

void nowaGraMenu()
{
	baner("STATKI");
	cout << endl << endl << "\t\t";
	cout << "Wybierz plansze:" << endl << "\t\t";
	cout << "1. Mala" << endl << "\t\t";
	cout << "2. Srednia" << endl << "\t\t";
	cout << "3. Duza" << endl << "\t\t";
	cout << "4. Wroc" << endl;
}

void nowaGra()
{
	int n;
	do
	{
		system("cls");
		nowaGraMenu();
		cin >> n;

		switch (n)
		{
		case 1: rozgrywka(n); break;
		case 2: rozgrywka(n); break;
		case 3: rozgrywka(n); break;
		case 4: break;
		}
	} while (n>4);
}

void wyczyscTablice()
{
	ofstream czysc;
	czysc.open("rekordy.txt");
	czysc.clear();
	czysc.close();
	cout << "Usunieto!" << endl;
}

void tablicaRekordow()
{
	system("cls");

	ifstream odczyt;
	string temp;
	char zn;
	odczyt.open("rekordy.txt");

	baner("STATKI");
	baner("REKORDY");

	if (!odczyt.good())
		cout << "Nie ma wynikow.\nNajpierw rozegraj gre wybierajac opcje 'Nowa Gra'"<<endl;
	else
	{
		cout << "Najlepsze wyniki:" << endl;
		while (!odczyt.eof())
		{
			getline(odczyt, temp);
			cout << temp << endl;
		}

		odczyt.close();

		cout << "Aby wyczyscic tablice rekordow nacisnij 'T' lub 'N' by pozostawic bez zmian: ";
		cin >> zn;
		if (zn == 'T' || zn == 't')
			wyczyscTablice();
	}
	system("pause");
}

void instrukcja()
{
	ifstream info;
	string temp;

	system("cls");
	info.open("instrukcja.txt");

	while (!info.eof())
	{
		getline(info, temp);
		cout << temp << endl;
	}

	system("pause");
	info.close();
}

void menu(int &n)
{
	do
	{
		system("cls");
		showMenu();
		cin >> n;

		switch (n)
		{
			case 1: nowaGra(); break;
			case 2: tablicaRekordow(); break;
			case 3: instrukcja(); break;
			case 4: break;
		}
	} while (n>4);
}
