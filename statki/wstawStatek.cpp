#include "stdafx.h"
#include "wstawStatek.h"

/*
kody ASCII:
- 13 - ENTER
- 27 - ESC
- 32 - SPACE
- 72 - strza�ka w g�r�
- 80 - strza�ka w d�
- 75 - strza�ka w lewo
- 77 - strza�ka w prawo
*/

void utworz(char **&tab, int n)
{
	tab = new char*[n];
	for (int i = 0; i < n; i++)
		tab[i] = new char[n];
}

void wypelnij(char **&tab, int n)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			tab[i][j] = '_';
}

void rysuj(char **tab, int n)
{
	cout << setw(3) << "|";
	for (int i = 0; i < n; i++)
	{
		cout << i << setw(1) << "|";
	}
	cout << endl;

	for (int i = 0; i < n; i++)
	{
		cout << setw(2) << i << setw(1) << "|";
		for (int j = 0; j < n; j++)
			cout << tab[i][j] << "|";
		cout << endl;
	}
}

void zapiszDane(int n)
{
	ofstream plansza;

	plansza.open("plansza.txt");

	switch (n)
	{
	case 1: plansza << "6" << endl;//wielko�� planszy n x n
		plansza << "4" << endl;//ilo�� statk�w
		plansza << "3\t2\t1\t1" << endl;//d�ugo�� poszczeg�lnych statk�w
		break;
	case 2: plansza << "8" << endl;//wielko�� planszy n x n
		plansza << "5" << endl;//ilo�� statk�w
		plansza << "4\t3\t2\t2\t1" << endl;//d�ugo�� poszczeg�lnych statk�w
		break;
	case 3: plansza << "10" << endl;//wielko�� planszy n x n
		plansza << "6" << endl;//ilo�� statk�w
		plansza << "5\t4\t3\t3\t2\t1" << endl;//d�ugo�� poszczeg�lnych statk�w
		break;
	}

	plansza.close();
}

void odczytDanych(SDane &dane)
{
	ifstream plansza;

	plansza.open("plansza.txt");

	plansza >> dane.n;
	plansza >> dane.ile;
	dane.s = new int[dane.ile];
	for (int i = 0; i < dane.ile; i++)
	{
		plansza >> dane.s[i];
		dane.pkt += dane.s[i];
	}

	plansza.close();
}

void dodajZnak(char **&tab, int x, int y, char zn)
{
	tab[y][x] = zn;
}

void przesun(char **&tab, int &x, int &y, SKursor &temp, int &tempX, int &tempY, int n)
{
	if (x == -1)
		x = n - 1;
	else if (x == n)
		x = 0;

	if (y == -1)
		y = n - 1;
	else if (y == n)
		y = 0;

	if (tab[y][x] == 'X')
	{
		tempX = x;
		tempY = y;
	}

	dodajZnak(tab, x, y, 219);
	if (!(tab[temp.y][temp.x] == 'X'))
		dodajZnak(tab, temp.x, temp.y, '_');
	temp.y = y;
	temp.x = x;
}

bool otoczenie(char **tab, int tempX, int tempY, int x, int y, int n)
{
	bool temp = false;
	//sprawdzi� czy podany punkt le�y obok wcze�niej wstawionego

	return temp;
}

void wstawStatki(char **&plansza)
{
	int zn = NULL, tempX = -1, tempY = -1, pomIle = 0, pomKtory = 0, pomCalosc = 0;
	SKursor temp, kursor;
	SDane dane;

	odczytDanych(dane);

	utworz(plansza, dane.n);
	wypelnij(plansza, dane.n);

	cout << "Wstaw swoja flote." << endl;
	cout << "Uzywajac strzalek poruszaj sie po planszy wybierajac kolejne wspolrzedne Twoich statkow." << endl;
	cout << "Dana wspolrzedna akceptuje sie naciskajac klawisz SPACE" << endl;
	cout << "Gdy wszystkie statki zostana wstawione nacisnij ENTER co zakonczy rozmieszczanie Twojej planszy" << endl;
	cout << "Masz do wstawienia:" << endl;
	for (int i = 0; i < dane.ile; i++)
		cout << "- " << dane.s[i] << " masztowiec" << endl;
	system("pause");

	while (pomCalosc != dane.pkt)
	{
		system("cls");
		cout << "Wstaw " << dane.s[pomKtory] << " masztowiec" << endl;
		system("pause");

		system("cls");
		dodajZnak(plansza, temp.x, temp.y, 219);
		rysuj(plansza, dane.n);

		if (tempX != -1 && tempY != -1)
			dodajZnak(plansza, tempX, tempY, 'X');

		while (pomIle != dane.s[pomKtory])
		{
			zn = _getch();

			switch (zn)
			{
				case 72:	przesun(plansza, kursor.x, --kursor.y, temp, tempX, tempY, dane.n);
							system("cls");
							rysuj(plansza, dane.n);
							break;//strza�ka w g�r�
				case 80:	przesun(plansza, kursor.x, ++kursor.y, temp, tempX, tempY, dane.n);
							system("cls");
							rysuj(plansza, dane.n);
							break;//strza�ka w d�
				case 75:	przesun(plansza, --kursor.x, kursor.y, temp, tempX, tempY, dane.n);
							system("cls");
							rysuj(plansza, dane.n);
							break;//strza�ka w lewo
				case 77:	przesun(plansza, ++kursor.x, kursor.y, temp, tempX, tempY, dane.n);
							system("cls");
							rysuj(plansza, dane.n);
							break;//strza�ka w prawo
				case 32:	if (plansza[kursor.y][kursor.x] != 'X')
							{
								dodajZnak(plansza, kursor.x, kursor.y, 'X');
								system("cls");
								rysuj(plansza, dane.n);
								pomIle++;
								pomCalosc++;
								break;
							}
							else
								break;
			}
			if (tempX != -1 && tempY != -1)
				dodajZnak(plansza, tempX, tempY, 'X');
		}

		if (plansza[temp.y][temp.x] == 'X')
		{
			tempX = temp.x;
			tempY = temp.y;
		}

		pomIle = 0;
		pomKtory++;
		system("pause");
	}

	system("cls");
	cout << "Rozmiesciles cala flote!\nCzas na gre!" << endl;
	rysuj(plansza, dane.n);
	system("pause");
}
