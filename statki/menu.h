#ifndef MENU
#define MENU

#include <iostream>
using namespace std;

void baner(string kto);
void showMenu();
void nowaGraMenu();
void nowaGra();
void wyczyscTablice();
void tablicaRekordow();
void instrukcja();
void menu(int &n);

#endif // !MENU
